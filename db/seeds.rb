# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.delete_all
Project.delete_all
Task.delete_all
@user = User.create(name: 'admin', email: 'admin@example.com', password: 'qwaszx12', password_confirmation: 'qwaszx12')
@project = @user.projects.create(name: 'Work')
@task = @project.tasks.create(name: "Prepare today's agenta")
@task = @project.tasks.create(name: "Contact John in HR")
@task = @project.tasks.create(name: "Complete Budget")
@task = @project.tasks.create(name: "Weekly Report")
@task = @project.tasks.create(name: "Define new product")
@task = @project.tasks.create(name: "Clean office plies", status: 'complete')
@project = @user.projects.create(name: 'House')
@task = @project.tasks.create(name: 'Deposit refund check')
@task = @project.tasks.create(name: 'Fill gas tank')
@task = @project.tasks.create(name: 'Pick up dry cleaning')
@task = @project.tasks.create(name: 'Mail Letters')
@task = @project.tasks.create(name: 'Yandex Tolstoy Startup Camp')
