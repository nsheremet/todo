class TodoController < ApplicationController

  COMPLETE_STATUS = 'complete'

  def index
    @user = User.find_by(id: session[:user_id])
  	@projects = @user.projects.order(:id)
  	@project = Project.new
    @tasks = Task.order(:id)
  end

  def new
  	@task = Task.new
  end

  def task_create
  	@project = Project.find(params[:project_id])
  	@task = @project.tasks.create(name: params[:name], status: nil )

    respond_to do |format|
      if @task.save
        format.html { redirect_to todo_path, notice: 'Task was successfully created.' }
        format.js
        format.json { render :show, status: :created, location: @task }
      else
        format.html { render :new }
        format.json { render json: @task.errors, status: :unprocessable_entity }
      end
    end
  end

  def task_destroy
    @task = Task.find(params[:task_id])
    @task.destroy
    respond_to do |format|
      format.html { redirect_to todo_path, notice: 'Task was successfully destroyed.' }
      format.js
      format.json { head :no_content }
    end
  end

  def task_status

    @task = Task.find(params[:id])

    if @task.status == COMPLETE_STATUS
      @task.status = nil
    else
      @task.status = COMPLETE_STATUS
    end

    @task.save!
  end

  def up_task

    @task = Task.find(params[:id])
    @task.id = @task.id - 1;
    @task.save!
    respond_to do |format|
      format.html { redirect_to todo_path }
      format.js
      format.json { head :no_content }
    end

  end

end
