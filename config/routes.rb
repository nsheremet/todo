Rails.application.routes.draw do
  get 'sessions/new'

  get 'sessions/create'

  get 'sessions/destroy'

  resources :users

  post 'todo/index' => 'todo#task_create'

  delete 'todo/index' => 'todo#task_destroy', as: 'destroy_task'

  delete 'projects/index' => 'projects#destroy', as: 'destroy_project'

  get 'task/:id/task_status' => 'todo#task_status'

  get 'todo/:task_id/up_task' => 'todo#up_task', as: 'up_task'

  get 'todo/:task_id/down_task' => 'todo#down_task', as: 'down_task'

  controller :todo do
    get 'add' => :task_new
  end

  controller :users do
    get 'registration' => :new, as: 'registration'
  end

  controller :sessions do
    get 'login' => :new
    post 'sessions/new' => :create
    delete 'logout' => :destroy
  end

  resources :projects

  resources :tasks

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  root to: 'todo#index', as: 'todo'
  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
